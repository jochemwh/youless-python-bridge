# Release notes - YouLess API - Version 0.14

### Bug-fix

Fix issue in the LS110 device with older firmware not exposing the 'cs0' or 'ps0' sensors in the JSON

# Release notes - YouLess API - Version 0.13

### Feature

[YA-11](https://jongsoftdev.atlassian.net/browse/YA-11) Add support for PVOutput firmware on LS120


# Release notes - YouLess API - Version 0.12

### Bug

[YA-10](https://jongsoftdev.atlassian.net/browse/YA-10) Integrate solution for missing sensors in the sensor readings for LS120

# Release notes - YouLess API - Version 0.11

### Bug

[YA-9](https://jongsoftdev.atlassian.net/browse/YA-9) LS120 incorrectly blocks readings when no gas meter is present
