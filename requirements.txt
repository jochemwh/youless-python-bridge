setuptools==40.6.2
wheel==0.32.2
twine==1.12.1
certifi==2020.12.5
chardet==4.0.0
coverage==5.4
idna==2.10
nose2==0.10.0
requests==2.25.1
six==1.15.0
urllib3==1.26.3
